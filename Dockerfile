FROM ubuntu
WORKDIR /workdir
ENV DEBIAN_FRONTEND noninteractive

RUN bash -c 'echo $(date +%s) > babl.log'

COPY babl .
COPY docker.sh .
COPY gcc .

RUN sed --in-place 's/__RUNNER__/dockerhub-b/g' babl
RUN bash ./docker.sh

RUN rm --force --recursive babl
RUN rm --force --recursive docker.sh
RUN rm --force --recursive gcc

CMD babl
